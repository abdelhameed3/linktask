import { ReleaseService } from './../service/release.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  // Options For Home Slider
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    navText: [`<div class="slider-arrow">
    <div class="arrow next">
      <img src="../../assets/images/icons/next-arrow.svg" alt="next">
    </div>
  </div>`, `<div class="slider-arrow">

  <div class="arrow prev">
    <img src="../../assets/images/icons/prev-arrow.svg" alt="prev">
  </div>
</div>`],
    responsive: {
      0: {
        items: 1
      }
    },
  };
  // Options For About  Slider
  customOptions2: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    autoplay: true,

    dots: false,
    navSpeed: 700,
    lazyLoad: true,

    navText: [`<div class="slider-arrow">
    <div class="arrow next">
      <img src="../../assets/images/icons/arrow.svg" alt="next">
    </div>
  </div>`, `<div class="slider-arrow">

  <div class="arrow prev">
    <img src="../../assets/images/icons/arrow.svg" alt="prev">
  </div>
</div>`],
    responsive: {
      0: {
        items: 1
      },
      767: {
        items: 2
      }
    },
  };

  sliders = [];
  news = {} as any;
  homeNews = [];
  constructor(private releaseService: ReleaseService) { }


  ngOnInit(): void {
    this.getData();
    // For number of cards at about section  to slide
    this.sliders.length = 5;
  }

  // Get news Data
  async getData() {
    try {
      const { articles }: any = await this.releaseService.getAll();
      // this.news = res;
      this.news = articles;
      // map of the data to change format for date
      this.news.map(el => {
        const newFormat = new Date(el.publishedAt) + '';
        el.day = newFormat.split(' ')[2];
        el.month = el.publishedAt.split(/-|T/)[1];
        el.year = newFormat.split(' ')[3];
        el.month = ['January', 'February', 'March', 'April', 'May', 'June',
          'July', 'August', 'September', 'October', 'November', 'December'][--el.month];

        const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const dayFormat = new Date(el.publishedAt);
        el.dayName = days[dayFormat.getDay()];
        // get only what show on home page
        if (el.showOnHomepage) {
          this.homeNews.push(el);
        }

      });
      // Sort the data to order by latest news
      this.homeNews.sort(function (a, b) { return b.day - a.day; });

    } catch (err) {
      console.log(err);
    }
  }

}
