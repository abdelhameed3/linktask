import { ReleaseService } from './../service/release.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-release-details',
  templateUrl: './release-details.component.html',
  styleUrls: ['./release-details.component.scss']
})
export class ReleaseDetailsComponent implements OnInit {

  newsId;
  news = {} as any;
  article = {} as any;
  items = [];
  interested = [
    { photo: '../../assets/images/news-1.png', title: 'About MRMI', description: 'Mohammed Bin Rashid Al Maktoum Global Initiatives was inaugurated on 4 October 2015. ' },
    { photo: '../../assets/images/news-2.png', title: 'Our Message', description: 'As a young Foundation, we are inspired by our young nation whose achievements have surpassed.' },
    { photo: '../../assets/images/news-3.png', title: 'Our Vision, Mission & Goals', description: 'VisionAl Jalila Foundation is a global philanthropic organisation.' },
  ]
  constructor(
    private activatedRoute: ActivatedRoute,
    private releaseService: ReleaseService
  ) { }

  ngOnInit(): void {
    this.newsId = this.activatedRoute.snapshot.params.id;
    this.getData();
    this.items.length = 5;
  }

  // Get news Data by id
  async getData() {
    try {
      const { articles }: any = await this.releaseService.getAll();
      this.news = articles;

      // map of the data to change format for date
      this.news.map(el => {
        const newFormat = new Date(el.publishedAt) + '';
        el.day = newFormat.split(' ')[2];
        el.month = el.publishedAt.split(/-|T/)[1];
        el.year = newFormat.split(' ')[3];
        el.month = ['January', 'February', 'March', 'April', 'May', 'June',
          'July', 'August', 'September', 'October', 'November', 'December'][--el.month];

      });
      console.log(this.news);

      // get By id
      if (this.newsId) {
        this.news = this.news.filter(item =>
          item.id + '' === this.newsId
        );
        this.article = this.news[0];
      }
      console.log(this.article);
    } catch (err) {
      console.log(err);
    }
  }
}
