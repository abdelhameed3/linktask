import { ReleaseDetailsComponent } from './release-details/release-details.component';
import { ReleasesComponent } from './releases/releases.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'press-release',
    data: {
      breadcrumb: 'Press Release'
    },
    component: ReleasesComponent,
  },
  {
    path: 'press-release/:id',
    component: ReleaseDetailsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
