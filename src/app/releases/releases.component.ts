import { ReleaseService } from './../service/release.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-releases',
  templateUrl: './releases.component.html',
  styleUrls: ['./releases.component.scss']
})
export class ReleasesComponent implements OnInit {

  news = {} as any;
  numberToShow = 0;
  newsNumber = 0;
  hideButton = false;
  categories = [];
  filter = {} as any;
  filterNews = [];
  constructor(private releaseService: ReleaseService) { }

  ngOnInit(): void {
    // to select the default option in select dropdown
    this.filter.category = 0;
    // the default number of show news in the page
    this.numberToShow = 8;
    this.getData(this.numberToShow);
  }

  // Get news Data
  async getData(number) {
    try {
      const res = await this.releaseService.getAll();
      this.news = res;

      // clone array in new one to use for filter options
      this.filterNews = [...this.news.articles];

      // this is to rest the select dropdown
      this.categories = [];
      // put data in dropdown as label and value
      this.news.sourceCategory.map(el => {
        this.categories.push({
          label: el.name,
          value: el.id
        });
      });

      // map of the data to change format for date
      this.filterNews.map(el => {
        const newFormat = new Date(el.publishedAt) + '';
        el.day = newFormat.split(' ')[2];
        el.month = el.publishedAt.split(/-|T/)[1];
        el.year = newFormat.split(' ')[3];
        el.month = ['January', 'February', 'March', 'April', 'May', 'June',
          'July', 'August', 'September', 'October', 'November', 'December'][--el.month];

        const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const dayFormat = new Date(el.publishedAt);
        el.dayName = days[dayFormat.getDay()];
      });
      // user this number to check if there a data or not
      this.newsNumber = this.filterNews.length;
      // Sort the data to order by latest news
      this.filterNews.sort(function (a, b) { return b.day - a.day; });
      // splice the array of data
      this.filterNews.splice(number);
      console.log(this.filterNews);
    } catch (err) {
      console.log(err);
    }
  }

  // load more logic
  loadMore() {
    this.numberToShow += 4;

    // To hide button load more from ui if there's no data to show
    if (this.numberToShow >= this.newsNumber) {
      this.hideButton = true;
    }

    this.getData(this.numberToShow);

  }

  // update using the value that the user input
  updateFilterNews() {
    this.filterNews = [...this.news.articles];
    if (this.filter.text || this.filter.category) {
      this.hideButton = true;
      this.filterByValue();
      this.filterByCategory();
    }
    console.log(this.filterNews);

  }

  filterByCategory() {
    if (this.filter.category) {
      this.filterNews = this.filterNews.filter(item =>
        item.sourceID + '' === this.filter.category + ''
      );
    }
  }

  filterByValue() {
    if (this.filter.text) {
      this.filterNews = this.filterNews.filter(item =>
        item.title.toLowerCase().includes(this.filter.text.toLowerCase())
      );
    }
  }
}
