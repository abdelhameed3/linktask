import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  @Input('name') set name(name) {
    this.pageName = name || null;
  };
  pageName = null;
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    const breadcrumb = this.route.snapshot.data['breadcrumb'];
    this.pageName = this.pageName || breadcrumb;

  }

}
